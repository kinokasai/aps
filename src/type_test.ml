open Ast
open Rresult
open Typecheck

let log f = Format.printf (f ^^ "@.")

let test_expr () =
  log "Test expr";
  let ctx = ctx_make () in 
  assert ((Typecheck.type_expr ctx True) = R.ok Bool);
  assert ((Typecheck.type_expr ctx False) = R.ok Bool);
  assert (type_expr ctx (Num "123") = R.ok Int);
  ()
