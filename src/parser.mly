%{
  open Ast
%}

%token SEMICOLON COLON STAR COMMA
%token LPAREN RPAREN LBRACKET RBRACKET
%token ARROW CONST FUN IF REC ECHO VAR PROC UPIF SET WHILE CALL
%token BOOL INT VOID
%token TRUE FALSE
%token EOF
%token <string> LITTERAL
%token <string> ID OPRIM

%start <Ast.prog> prog
%%

arg_list:
  | argl = separated_list(COMMA, arg = arg { arg }) { argl }

arg:
  | id = ID COLON type_t = type_t { {id; kind = type_t}}

expr:
  | TRUE { True }
  | FALSE { False }
  | num = LITTERAL { Num(num) }
  | id = ID { Id(id) }
  | LPAREN op=OPRIM exprs = list(expr) RPAREN { Oprim(op, exprs)}
  | LPAREN IF cond = expr true_alt = expr false_alt = expr RPAREN { If(cond, true_alt, false_alt) }
  | LBRACKET args = arg_list RBRACKET body = expr { Lambda(args, body) }
  | LPAREN id=expr args= list(expr) RPAREN { Application(id, args) }

type_list:
  | typel = separated_list(STAR, type_t = type_t { type_t }) { typel }

type_t:
  | INT { Int }
  | BOOL { Bool }
  | VOID { Void }
  | LPAREN typel = type_list ARROW ret_type = type_t RPAREN { FuncType(typel, ret_type) }

stat:
  | ECHO expr=expr { Echo(expr) }
  | SET id=ID expr=expr { Set(id, expr) }
  | UPIF expr=expr left=prog right=prog { ImpIf(expr, left, right) }
  | WHILE cond=expr body=prog { While(cond, body) }
  | CALL id=ID args=list(expr) { Call(id, args )}

dec:
  | CONST id=ID kind=type_t expr=expr { Const(id, kind, expr) }
  | FUN id=ID kind=type_t LBRACKET args=arg_list RBRACKET expr=expr {Func(id, kind, args, expr)}
  | FUN REC id=ID kind=type_t LBRACKET args=arg_list RBRACKET expr=expr { FuncRec(id, kind, args, expr)}
  | VAR id=ID kind=type_t { VarDec(id, kind) }
  | PROC id=ID LBRACKET args=arg_list RBRACKET body=prog { Proc(id, args, body) }
  | PROC REC id=ID LBRACKET args=arg_list RBRACKET body=prog { ProcRec(id, args, body) }

cmd_list:
  | cmdl = separated_list(SEMICOLON, cmd = cmd { cmd }) {cmdl}

cmd:
  | stat=stat { Stat(stat) }
  | dec=dec { Dec(dec) }

prog:
  | LBRACKET cmds = cmd_list RBRACKET EOF {{cmds}}
(*
init:
  | tdl = type_dec_list nl = node_list EOF {{type_dec_list = tdl; node_list = nl}}

rhs:
  | cexp = cexp { CExp(cexp) }
  | pre = value FBY next = exp { Fby(pre, next)}
  | id = ID LPAREN expl = exp_list RPAREN EVERY UNDERSCORE { NodeCall(id, expl)}

cexp:
  | exp = exp { Exp(exp) }
  | MERGE id = ID fl = flow_list { Merge(id, fl)}

constr:
    | id = CONSTR LPAREN idl = separated_list(COMMA, ID) RPAREN {{id; params=idl}}
    | id = CONSTR {{id; params=[]}}

eq_list:
  | eql = separated_list(SEMICOLON, eq = eq { eq }) { eql }

eq:
  | lhs = ID EQUALS rhs = rhs { {lhs = [lhs]; rhs} }
  | LPAREN lhs = separated_list(COMMA, ID) RPAREN EQUALS rhs = rhs {{lhs; rhs}}

exp_list:
  | expl = separated_list(COMMA, exp = exp { exp }) { expl }

exp:
  | id = ID LPAREN expl = exp_list RPAREN {Op(id, expl)}
  | id = ID { Variable(id)}
  | vl = value { Value(vl) }
  | exp = exp WHEN constr { When(exp) }

flow_list:
  | fll = list(fl = flow { fl }) { fll }

flow:
  | LPAREN constr = constr RETURNS cexp = cexp RPAREN { {constr; cexp} }

node_list:
  | nl = list(n = node {n}) { nl }

node:
  | interface = boption(INTERFACE) NODE id = ID LPAREN in_vdl = var_decs RPAREN 
    RETURNS LPAREN out_vdl = var_decs RPAREN WITH
    eql = eq_list { {interface; id; in_vdl; out_vdl; step_vdl = []; eql} }

type_dec_list:
    | tdl = list(type_dec) { tdl }

type_dec:
    | TYPE id = ID EQUALS type_list = separated_list(PIPE, ty) { {id; type_list} }

ty:
    | id = CONSTR LPAREN vdl = var_decs RPAREN { {id; vdl} }
    | id = CONSTR { {id; vdl = []} }

value_list:
  | vll = separated_list(COMMA, value) { vll }

value:
  | constr = constr {Constr(constr)}
  | lit = LITTERAL { Litteral(lit) }

var_decs:
  | vdl = separated_list(COMMA, var_dec) { vdl }

var_dec:
  | var_id = ID COLON type_id = ID { {var_id; type_id} }
*)