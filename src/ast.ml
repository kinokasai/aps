type type_t =
  | Bool
  | Int
  | Void
  | FuncType of type_t list * type_t

type arg = 
  {id: string; kind: type_t}

type expr =
  | True
  | False
  | Num of string
  | Id of string
  | Oprim of string * expr list
  | If of expr * expr * expr
  | Lambda of arg list * expr (* [x : int, y : int ] (+ x y) *)
  | Application of expr * expr list

type stat =
  | Echo of expr
  | Set of string * expr
  | ImpIf of expr * prog * prog
  | While of expr * prog
  | Call of string * expr list

and dec =
  | Const of string * type_t * expr
  | Func of string * type_t * arg list * expr
  | FuncRec of string * type_t * arg list * expr
  | VarDec of string * type_t
  | Proc of string * arg list * prog
  | ProcRec of string * arg list * prog

and cmd =
  | Stat of stat
  | Dec of dec

and prog = {cmds: cmd list}