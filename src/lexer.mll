{
  open Parser
  exception SyntaxError of string
}

let digit = ['0'-'9']
let white_ = [' ' '\t' '\n']+
let int = digit+
let float = digit+ ('.'digit*)?
let ident = ['a'-'z''A'-'Z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let type_const = ['A'-'Z']['a'-'z' 'A'-'Z']*
let comment = '#'([^ '\n' ]*)
let num = '-'? ['0'-'9']+

rule token = parse
    | white_ { token lexbuf }
    | ';' { SEMICOLON }
    | ',' { COMMA }
    | '(' { LPAREN }
    | ')' { RPAREN }
    | '[' { LBRACKET }
    | ']' { RBRACKET }
    | ':' { COLON }
    | '*' { STAR }
    | "add" as op { OPRIM op}
    | "not" | "or" | "add" | "sub" | "mul" | "div" | "eq" | "lt" as op { OPRIM op }
    | comment { token lexbuf }
    | "if" { IF }
    | "int" { INT }
    | "bool" { BOOL }
    | "void" { VOID }
    | "CONST" { CONST }
    | "FUN" { FUN }
    | "REC" { REC }
    | "ECHO" { ECHO }
    | "VAR" { VAR }
    | "PROC" { PROC }
    | "SET" { SET }
    | "IF" { UPIF }
    | "WHILE" { WHILE }
    | "CALL" { CALL }
    | "true" { TRUE }
    | "false" { FALSE }
    | "->" { ARROW }
    | num as num { LITTERAL num}
    | ident as id { ID id }
    | eof { EOF }
    | _
      { raise (SyntaxError ("Syntax Error: " ^ Lexing.lexeme lexbuf))}