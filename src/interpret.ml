open Ast

module List = Base.List
module Int = Core.Int
module Map = Base.Map

exception UnkownOp of string
exception TypeError of string
exception NotImplemented

type env =  value Core.String.Map.t
and memory = value Core.Int.Map.t
and ctx = {env: env; mem: memory}
and cls = {body: expr; params: string list; clsenv: env}
and value =
  | N of int
  | F of cls
  | FR of string * cls (* Because we remember the name *)
  | Addr of int

let make_closure body params clsenv =
  {body; params; clsenv}

let envset ctx key data =
  {ctx with env= Map.set ctx.env key data}

let memset ctx key data =
  {ctx with mem = Map.set ctx.mem key data}

let envget ctx key =
  Map.find_exn ctx.env key

let memget ctx key =
  Map.find_exn ctx.mem key

let alloc = 
  let idx = ref ~-1 in
  (fun () -> incr idx; !idx)

let (>>=) m f =
  match m with
  | N a -> N (f a)
  | _ -> m

let (>>=@) m f =
  match m with
  | F cls -> F (f cls)
  | _ -> m

let get_int m =
  match m with
  | N a -> a
  | F(_) -> raise (TypeError "Called get_int on function")
  | _ ->  raise (TypeError "Called get_int on function")

let get_cls v =
  match v with
  | F(cls) -> cls
  | _ -> raise (TypeError "Called get_cls on int")

let get_rcls v =
  match v with
  | FR(id, cls) -> id, cls
  | _ -> raise (TypeError "Expected recursive closure.")

let recurse value id =
  match value with
  | F(cls) -> FR(id, cls)
  | _ -> value

let tru = N 1
let fals = N 0


let rec interpret_oprim ctx op args =
  let arg_values = args |> List.map ~f:(interpret_expr ctx) in
  let lhs = (List.nth_exn arg_values 0) in
  let rhs = get_int (List.nth_exn arg_values 1) in
  match op with
  | "add" -> lhs >>= (+) rhs
  | "sub" -> lhs >>= (-) rhs
  | "mul" -> lhs >>= ( * ) rhs
  | "div" -> lhs >>= (/) rhs
  | "not" -> lhs >>= (function |0 -> 1 |_ -> 0)
  | "eq" -> lhs >>= (function |i when i = rhs -> 1 |_ -> 0)
  | "lt" -> if get_int lhs < rhs then tru else fals
  | _ -> raise (UnkownOp op)

and interpret_if ctx cond alt_true alt_false =
  match interpret_expr ctx cond with
  | N 0 -> interpret_expr ctx alt_false
  | _ -> interpret_expr ctx alt_true

and interpret_abstraction ctx params body =
  F (make_closure body (params |> List.map ~f:(fun param -> print_endline @@ "capturing " ^ param.id; param.id)) ctx.env)

and interpret_simple_application ctx expr args =
  let closure = interpret_expr ctx expr |> get_cls in
  let new_env = args
    |> List.map ~f:(interpret_expr ctx)
    |> (List.fold2_exn ~f:(fun acc name value -> Core.Map.set acc name value) ~init:closure.clsenv closure.params) in
  interpret_expr {ctx with env = new_env} closure.body

and interpret_rec_application ctx id expr args =
  let id,closure = interpret_expr ctx expr |> get_rcls in
  let new_env = args
    |> List.map ~f:(interpret_expr ctx)
    |> (List.fold2_exn ~f:(fun acc name value -> Core.Map.set acc name value) ~init:closure.clsenv closure.params) in
  let new_env = (Core.Map.set new_env ~key:id ~data:(FR (id, closure))) in
  interpret_expr {ctx with env = new_env} closure.body
  
and interpret_id ctx id =
  match Map.find_exn ctx.env id with
  | Addr(value) -> Map.find_exn ctx.mem value
  | _ as value -> value

and interpret_expr ctx expr = 
  match expr with
  | True -> tru
  | False -> fals
  | Num(n) -> N (int_of_string n)
  | Id(id) -> interpret_id ctx id
  | Oprim(op, args) -> interpret_oprim ctx op args
  | If(cond, alt_true, alt_false) -> interpret_if ctx cond alt_true alt_false
  | Lambda(params, body) -> interpret_abstraction ctx params body
  | Application(expr, args) -> (
      match interpret_expr ctx expr with
      | F _ -> interpret_simple_application ctx expr args
      | FR(id, _) -> interpret_rec_application ctx id expr args
      | _ -> raise @@ TypeError "Expected closure")

and interpret_stat ctx = function
  | Echo(expr) -> expr |> interpret_expr ctx |> get_int |> print_int; ctx
  | Set(id, expr) -> (match envget ctx id with | Addr(a) -> memset ctx a (interpret_expr ctx expr) | _ -> raise @@ TypeError "lol")
  | _ -> raise NotImplemented

and interpret_dec ctx = function
  | Const(id,_,expr) ->
    let value = (expr |> interpret_expr ctx) in
      envset ctx id value
  | Func(id,_,params,body) ->
    let lambda_val = interpret_abstraction ctx params body in
      envset ctx id lambda_val
  | FuncRec(id,_,params,body) ->
    let lambda_val = interpret_abstraction ctx params body in
      envset ctx id (recurse lambda_val id)
  | VarDec(id, _) -> envset ctx id (Addr(alloc()))
  | _ -> raise NotImplemented

and interpret_cmds ctx cmds =
  match cmds with
  | [] -> ()
  | Stat(st)::l -> interpret_cmds (interpret_stat ctx st) l
  | Dec(dec)::l -> interpret_cmds (interpret_dec ctx dec) l

and interpret_prog prog =
  let env = Core.Map.empty (module Base.String) in
  let mem = Core.Map.empty (module Int) in
  interpret_cmds {env; mem} prog.cmds

let _ = ()
  (* let map = Core.String.Map.empty in
  assert(interpret_expr map (Num "1") = N 1);
  assert(interpret_expr map (Oprim("add", [Num "1"; Num "2"])) = N 3);
  let op = Oprim("add", [Num "1"; Id("x")]) in
  assert(interpret_expr map (Application(Lambda([{id="x"; kind=Int}], op), [Num "1"])) = N 2) *)