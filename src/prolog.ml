open Ast
open BatList

let concat = String.concat

let from_id id = "\"" ^ id ^ "\""

let rec from_exprs exprl = exprl |> map from_expr |> concat ","

and from_expr = function
  | True -> "true"
  | False -> "false"
  | Num(num) -> num
  | Id(id) -> from_id id
  | Oprim(op, exprl) -> op ^ "(" ^ from_exprs exprl ^ ")"
  | If(cond, true_alt, false_alt) -> "if(" ^ from_expr cond ^ "," ^ from_expr true_alt ^ "," ^ from_expr false_alt ^ ")"
  | Lambda(args, body) -> "lambda(" ^ from_args args ^ ", " ^ from_expr body ^ ")"
  | Application(id, args) -> "app(" ^ from_expr id ^ ", " ^ from_exprs args ^ " )"

and from_arg arg = "ass(" ^ from_id arg.id ^ ", " ^ from_type arg.kind ^ ")"
and from_args args = "[ " ^ (args |> map from_arg |> concat ",") ^ "]"

and from_type = function
  | Int -> "int"
  | Bool -> "bool"
  | Void -> "Void"
  | FuncType(param_types, ret_ty) -> "fn_t([" ^ (param_types |> map from_type|> concat ",")
                                     ^ "], " ^ from_type ret_ty ^ ")"

and from_dec = function
  | Const(id, ty, expr) -> "const(" ^ from_id id ^ ", " ^ from_type ty ^ ", " ^ from_expr expr ^ ")"
  | Func(id, ty, args, expr) -> "func(" ^ from_id id ^ ", " ^ from_type ty ^ ", " ^ from_args args ^ ", " ^ from_expr expr ^ ")"
  | FuncRec(id, ty, args, expr) -> "funcrec(" ^ from_id id ^ ", " ^ from_type ty ^ ", " ^ from_args args ^ ", " ^ from_expr expr ^ ")"

and from_stat = function
  | Echo(expr) -> "echo(" ^ from_expr expr ^ ")"

and from_cmd = function
  | Stat(stat) -> from_stat stat
  | Dec(dec) -> from_dec dec

and from_prog prog =
  "[" ^ (prog.cmds |> map from_cmd |> concat ",") ^ "]"