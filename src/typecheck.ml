open Ast
open Rresult
open Base

exception NotImplemented
type context = type_t Core.String.Map.t

let processed = ref "";;

let get_map ctx id =
  match Core.Map.find ctx id with
  | None -> R.error ("Variable " ^ id ^ " is not in context.")
  | Some(ty) -> R.ok ty

let ctx_add ctx id kind =
  Map.set ctx id kind

let ctx_make () =
  let primitives = [("add", FuncType([Int; Int], Int));
                    ("sub", FuncType([Int; Int], Int));
                    ("mul", FuncType([Int; Int], Int));
                    ("div", FuncType([Int; Int], Int));
                    ("not", FuncType([Bool; Bool], Bool));
                    ("eq", FuncType([Int; Int], Bool));
                    ("lt", FuncType([Int; Int], Bool))
                    ] in
  (* Map.of_alist_multi (Base.Comparator.make ~compare:String.compare ~sexp_of_t:String.sexp_of_t) primitives *)
  Map.of_alist_exn (module String) primitives

let rec fold_error = function
  | [] -> R.ok Int (* Int is a hack - the result will be discarded *)
  | result::[] -> result
  | result::l when R.is_ok(result) -> fold_error l
  | result::l -> result

let (>>&) first second =
  match (first, second) with
  | (Error(_) as err, _) -> err
  | (Ok(_), (Error(_) as err)) -> err
  | (Ok(_), (Ok(_) as ok)) -> ok

let (&->) result e =
  match (result) with
  | Error(_) as err -> err
  | Ok(_) -> Ok(e)
  
let rec type_oprim ctx op exprl =
  match Map.find ctx op with
  | None -> R.error ("Function " ^ op ^ " not in context.")
  | Some(ty) -> type_application ctx exprl ty

and type_application ctx exprl func_ty =
  match func_ty with
  | FuncType(arg_tys, ret_ty) ->(
      match check_func_type ctx arg_tys exprl with
      | Ok(_) -> R.ok ret_ty
      | Error(_) as err -> err)
  | _ -> R.error ("Function has incoherent type")

and type_lambda ctx params body =
  let new_ctx = params
    |> List.fold_left ~f:(fun acc param -> ctx_add acc param.id param.kind) ~init:ctx in
  let args_ty = params |> List.map ~f:(fun arg -> arg.kind) in
  let result_ret_ty = type_expr new_ctx body in
  match result_ret_ty with
  | Error(_) as err -> err
  | Ok(ret_ty) -> R.ok (FuncType(args_ty, ret_ty))

and check_func_type ctx args_tys exprl =
  List.map2_exn exprl args_tys ~f:(check_expr ctx) 
    |> fold_error

and check_expr ctx expr expected_ty =
  let f = (fun ty -> if Caml.Pervasives.(=) ty expected_ty then R.ok ty
                                          else R.error ("[" ^ !processed ^ "] Inequal types: " ^ Prolog.from_type ty ^ " | " ^ Prolog.from_type expected_ty)) in
  type_expr ctx expr >>= f

and type_expr ctx = function
  | True -> R.ok Bool
  | False -> R.ok Bool
  | Num(_) -> R.ok Int
  | Id(id) -> get_map ctx id
  | Oprim(op, exprl) ->  type_oprim ctx op exprl
  | Application(f, exprl) -> type_expr ctx f >>= type_application ctx exprl
  | If(cond, alt_true, alt_false) -> 
      check_expr ctx cond Bool
      >>& type_expr ctx alt_true
      >>= check_expr ctx alt_false
  | Lambda(params, body) -> type_lambda ctx params body

(* We'll return the context in cmds *)
and type_stat ctx = function
  | Echo(expr) -> check_expr ctx expr Int >>& Ok(Void)
  | Set(id, expr) -> get_map ctx id >>= check_expr ctx expr >>& Ok(Void)
  | ImpIf(cond, left, right) -> check_expr ctx cond Bool >>& type_cmds ctx left.cmds >>& type_cmds ctx right.cmds
  | While(cond, body) -> check_expr ctx cond Bool >>& type_cmds ctx body.cmds
  | Call(id, args) -> get_map ctx id >>= type_application ctx args 

and check_stat ctx st expected_ty =
  let f = (fun ty -> if Caml.Pervasives.(=) ty expected_ty then R.ok ty else R.error "Inequal types") in
  type_stat ctx st >>= f

and type_func ctx id ty args expr =
  let new_ctx = args
    |> List.fold_left ~init:ctx ~f:(fun acc arg -> ctx_add acc arg.id arg.kind)
  and func_args_ty = args |> List.map ~f:(fun arg -> arg.kind) in
  let func_ctx = ctx_add ctx id (FuncType(func_args_ty, ty)) in
  check_expr new_ctx expr ty >>& Ok(func_ctx)

and type_funcrec ctx id ty args expr =
  let func_args_ty = args |> List.map ~f:(fun arg -> arg.kind) in
  let func_ty = FuncType(func_args_ty, ty) in
  let func_ctx = ctx_add ctx id func_ty in
  let new_ctx = args
    |> List.fold_left ~init:func_ctx ~f:(fun acc arg -> ctx_add acc arg.id arg.kind) in
  check_expr new_ctx expr ty >>& Ok(func_ctx)

  (* We add processed for some reason *)
and check_dec ctx = function
  | Const(id, ty, expr) -> processed := id; check_expr ctx expr ty >>& Ok(ctx_add ctx id ty)
  | Func(id, ty, args, expr) -> processed := id; type_func ctx id ty args expr
  | FuncRec(id, ty, args, expr) -> processed := id; type_funcrec ctx id ty args expr
  | VarDec(id, ty) -> processed := id; Ok(ctx_add ctx id ty)
  | Proc(id, args, body) -> processed := id; type_cmds ctx body.cmds >>& Ok(ctx_add ctx id Void)
  | ProcRec(id, args, body) -> processed := id; type_cmds ctx body.cmds >>& Ok(ctx_add ctx id Void)

and type_cmds ctx cmds =
  match cmds with
  | [] -> R.ok Void
  | Stat(st)::l -> check_stat ctx st Void >>& type_cmds ctx l
  | Dec(dec)::l -> (match check_dec ctx dec with
                    | Error(_) as err -> err
                    | Ok(new_ctx) -> type_cmds new_ctx l)

and type_prog prog =
  type_cmds (ctx_make()) prog.cmds