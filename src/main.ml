(* open Core.Std *)
open Lexer
open Parser
open Colors
open Batteries
open Printexc
open Prolog
open Type_test
open Typecheck
open Result
open Interpret

(* Pretty print errors *)
let lexsub start end_ str lexeme =
    let head = String.sub str 0 start and
        tail = String.sub str end_ (40) and
        red = "\027[32m" and
        endc = "\027[0m" in
    head ^ red ^ lexeme ^ endc ^ tail

let parse filename =
  let _ = print_endline ("parsing " ^ filename) in
  let input = File.open_in filename in
  let filebuf = Lexing.from_channel input in
  try
    let ast = Parser.prog Lexer.token filebuf in
    print_string (Prolog.from_prog ast);
    ast
  with
    | Parser.Error ->
        (Printf.eprintf "At offset %d: syntax error on token: \"%s\"!\n%s\n"
        (Lexing.lexeme_start filebuf) (Lexing.lexeme filebuf)
        (*(lexsub (filebuf.Lexing.lex_start_pos) filebuf.Lexing.lex_buffer (Lexing.lexeme filebuf));*)
        (lexsub (Lexing.lexeme_start filebuf) (Lexing.lexeme_end filebuf) (Bytes.to_string filebuf.Lexing.lex_buffer) (Lexing.lexeme filebuf));
        exit 2)
    | SyntaxError(str) ->
        print_string str |> exit 3

let type_ast ast =
    match type_prog ast with
    | Ok(Void) -> print_endline "Typechecking OK"; ast
    | Ok(_) -> print_endline "Wrong type"; ast
    | Error(msg) -> print_endline msg; raise @@ TypeError msg
    
(*         
let command =
    Command.basic
        ~summary:"Interpreter for APS0"
        Command.Spec.(
            empty
            +> flag "-o" (optional_with_default "out.pl" string) ~doc:" File output name"
            +> anon ("filename" %: file)
        )
    (fun out_name filename _ ->
        try
            let _ = Sys.command "mkdir -p build" in
            parse filename
        with
          (* | Unrecoverable -> "Unrecoverable Error: exiting..." |> print_endline *)
          | e -> print_string(to_string(e) ^ get_backtrace()); exit 4
    )

let _ =
    Command.run command *)

let file_string_of_int n =
    match n with
    | n when n < 10 -> "00" ^ (string_of_int n)
    | n when n < 100 -> "0" ^ (string_of_int n)
    | _ -> string_of_int n

let rec test_files n =
    match n with
    | 19 -> ()
    | 18 -> test_files (n + 1)
    | n -> parse ("tests/prog" ^ file_string_of_int n ^ ".aps") |> type_ast |> interpret_prog;
            print_endline "";
            test_files (n+1)

let _ =
  test_expr ();
  test_files 0
  